python cffi interface to yeppp math library
=====================================================

This repo is just a little bit of code to get `libyeppp` running with `python` using `cffi`.  It assumes you have already downloaded `yeppp` for your system and have it installed somewhere in your library search path.

> Yeppp! is a high-performance SIMD-optimized mathematical library for x86, ARM, and MIPS processors on Windows, Android, Mac OS X, and GNU/Linux systems. Yeppp! contains versions of its functions for multiple architectures and instruction sets and chooses the optimal implementation during initialization to guarantee the best performance on the host machine.

* [yeppp](http://www.yeppp.info) - Very fast single-core C math library.
* [yeppp git repo](https://bitbucket.org/MDukhan/yeppp/src)
* [cffi](https://bitbucket.org/cffi/cffi) - foreign function interface for python.  It comes built-in to pypy, but easily pip-installable for other pythons.

Example - Multiply
==================
* See `example_multiply.py` for a demonstration of
    - using cffi to allocate some memory
    - calling libyeppp to multiply (in-place) the array by a scalar


Creating header file
====================
* `cffi` still isn't great at parsing out *#define*s and *#ifdef*s
* Included is a small script (`build_header.py`) which parses all the original yeppp header files and extracts all the function declarations into a single header: `libyeppp.h`

Notes
=====
* Has only been tested on OSX.
* For a different platform:
    - Fix basic types
        - Run `echo '#include "headers/yepTypes.h"' | gcc -E - | grep -E "typedef.*(Yep8s|Yep8u|Yep16s|Yep16u|Yep32f|Yep32s|Yep32u|Yep64f|Yep64s|Yep64u|YepSize)` to figure out the types for your system
        - Edit these into `build_header/libyeppp_top.h`
    - Translate function *#define*s correctly.
        - run `echo '#include "headers/yepPredefines.h"' | gcc -dM -E - | grep -E "(YEP_PUBLIC_SYMBOL|YEP_IMPORT_SYMBOL|YEPABI|YEP_RESTRICT)"` to figure out what these defines should be.
        - edit these replacement strings into `build_header.py`
        - run `build_header.py` to generate the final `libyeppp.h`

Simple C code example - CpuInfo
================================
* Use this small example program to check whether yeppp is installed and working on your machine.
* `cd cpuinfo` and `make`
* If all goes well, then run `CpuInfo` to get detailed info on your CPU.

References
==========
* Helpful notes from someone else going through creation of a CFFI wrapper for a library: [Python interface to signalfd using FFI](http://blog.oddbit.com/2013/11/28/a-python-interface-to-signalfd/)

