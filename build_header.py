#!/usr/bin/env python
from __future__ import print_function
import os
import re

# Parse out just the function names from the original header files in order
# to make a combined header file 'libyeppp.h'

# This works on OSX Yosemite, but might need tweaking on other platforms.
# In particular, the particular replacement strings for the following are
# heavily dependant on compiler and platform
# - YEP_PUBLIC_SYMBOL
# - YEPABI
# - YEP_RESTRICT
#
# Run the following to work out the replacements on your platform:
# echo '#include "headers/yepPredefines.h"' | gcc -dM -E - | grep -E "(YEP_PUBLIC_SYMBOL|YEP_IMPORT_SYMBOL|YEPABI|YEP_RESTRICT)"
#

with open("build_header/libyeppp_bottom.h", "w") as out:
    out.write("/* The following code is built by running `build_header.py` */\n")
    for header in ['yepLibrary.h', 'yepAtomic.h', 'yepCore.h', 'yepMath.h', 'yepRandom.h']:
        out.write("/* " + header + " */\n")
        with open("headers/" + header) as h:
            text = h.read()
            public_functions = re.findall("(YEP_PUBLIC_SYMBOL.*?;)", text)
            for function in public_functions:
                # This replacement works on OSX.  YMMV. See notes at top of this file
                function = function.replace("YEP_PUBLIC_SYMBOL", "")
                function = function.replace("YEPABI"           , "")
                function = function.replace("YEP_RESTRICT"     , "restrict")
                out.write(function.strip() + "\n")
            out.write("\n")

os.system("cat build_header/libyeppp_top.h build_header/libyeppp_bottom.h > libyeppp.h")
