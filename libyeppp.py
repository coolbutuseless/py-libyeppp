#!/usr/bin/env python
from __future__ import print_function, division, absolute_import

import os
from cffi import FFI

ffi = FFI()

libyeppp_dir = os.path.dirname(os.path.abspath(__file__))
with open(libyeppp_dir + "/libyeppp.h") as header:
    ffi.cdef(header.read())

# Wherever you put libyeppp.dylib, make sure it's in your library search path
# e.g. OSX: export DYLD_FALLBACK_LIBRARY_PATH="/opt/local/lib"
# Or move it into the same directory as this file.
libyeppp = ffi.dlopen("libyeppp.dylib")

# library needs to be initialised
status = libyeppp.yepLibrary_Init();
assert status == libyeppp.YepStatusOk

