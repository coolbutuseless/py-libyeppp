/*
 *                          Yeppp! library header
 *
 * This file is part of Yeppp! library and licensed under the New BSD license.
 *
 * Copyright (C) 2010-2012 Marat Dukhan
 * Copyright (C) 2012-2013 Georgia Institute of Technology
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Georgia Institute of Technology nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* generate specific types for your platform by running:
echo '#include "headers/yepTypes.h"' | gcc -E - | grep -E "typedef.*(Yep8s|Yep8u|Yep16s|Yep16u|Yep32f|Yep32s|Yep32u|Yep64f|Yep64s|Yep64u|YepSize)
*/
typedef int8_t   Yep8s;
typedef uint8_t  Yep8u;
typedef int16_t  Yep16s;
typedef uint16_t Yep16u;
typedef float    Yep32f;
typedef int32_t  Yep32s;
typedef uint32_t Yep32u;
typedef double   Yep64f;
typedef int64_t  Yep64s;
typedef uint64_t Yep64u;
typedef size_t   YepSize;

/* Structs */
enum YepStatus {
 YepStatusOk = 0,
 YepStatusNullPointer = 1,
 YepStatusMisalignedPointer = 2,
 YepStatusInvalidArgument = 3,
 YepStatusInvalidData = 4,
 YepStatusInvalidState = 5,
 YepStatusUnsupportedHardware = 6,
 YepStatusUnsupportedSoftware = 7,
 YepStatusInsufficientBuffer = 8,
 YepStatusOutOfMemory = 9,
 YepStatusSystemError = 10,
 YepStatusAccessDenied = 11
};

struct YepLibraryVersion {
    /** @brief The major version. Library releases with the same major versions are guaranteed to be API- and ABI-compatible. */
    Yep32u major;
    /** @brief The minor version. A change in minor versions indicates addition of new features, and major bug-fixes. */
    Yep32u minor;
    /** @brief The patch level. A version with a higher patch level indicates minor bug-fixes. */
    Yep32u patch;
    /** @brief The build number. The build number is unique for the fixed combination of major, minor, and patch-level versions. */
    Yep32u build;
    /** @brief A UTF-8 string with a human-readable name of this release. May contain non-ASCII characters. */
    const char* releaseName;
};

enum YepCpuArchitecture {
    /** @brief      Instruction set architecture is not known to the library. */
    /** @details    This value is never returned on supported architectures. */
    YepCpuArchitectureUnknown = 0,
    /** @brief      x86 or x86-64 ISA. */
    YepCpuArchitectureX86 = 1,
    /** @brief      ARM ISA. */
    YepCpuArchitectureARM = 2,
    /** @brief      MIPS ISA. */
    YepCpuArchitectureMIPS = 3,
    /** @brief      PowerPC ISA. */
    YepCpuArchitecturePowerPC = 4,
    /** @brief      IA64 ISA. */
    YepCpuArchitectureIA64 = 5,
    /** @brief      SPARC ISA. */
    YepCpuArchitectureSPARC = 6
};

struct YepEnergyCounter {
        Yep64u state[6];
};

struct YepRandom_WELL1024a {
    Yep32u state[32];
    Yep32u index;
};
typedef struct YepRandom_WELL1024a random_state;

