#!/usr/bin/env python
from __future__ import print_function, division
from cffi import FFI

from libyeppp import libyeppp
ffi = FFI()

#=========================================================
# In-place multiplication of an array of doubles
#=========================================================
# Create a length N array of doubles in memory using ffi
N = 10
arr = ffi.new("double[]", N)

# Fill with numbers 0..N-1
for i in range(N):
    arr[i] = i

print("Arry of double * 2")
print("Initial array:", [arr[i] for i in range(N)])

# yepp function for in-place multiplication of a vector of doubles by a scalar:
# enum YepStatus  yepCore_Multiply_IV64fS64f_IV64f(Yep64f *restrict x, Yep64f y, YepSize length);
libyeppp.yepCore_Multiply_IV64fS64f_IV64f(arr, 2, N)

print("Array *=2    :", [arr[i] for i in range(N)])

#=========================================================
# In-place multiplication of an array of int8_t
#=========================================================
# Create a length N array of doubles in memory using ffi
N = 10
arr = ffi.new("int8_t[]", N)

# Fill with numbers 0..N-1
for i in range(N):
    arr[i] = i

print("Arry of int8_t * 2")
print("Initial array:", [arr[i] for i in range(N)])

# yepp function for in-place multiplication of a vector of uint8_t by a scalar:
# enum YepStatus  yepCore_Multiply_IV8sS8s_IV8s(Yep8s *restrict x, Yep8s y, YepSize length);
libyeppp.yepCore_Multiply_IV8sS8s_IV8s(arr, 2, N)

print("Array *=2    :", [arr[i] for i in range(N)])
