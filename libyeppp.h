/*
 *                          Yeppp! library header
 *
 * This file is part of Yeppp! library and licensed under the New BSD license.
 *
 * Copyright (C) 2010-2012 Marat Dukhan
 * Copyright (C) 2012-2013 Georgia Institute of Technology
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Georgia Institute of Technology nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* generate specific types for your platform by running:
echo '#include "headers/yepTypes.h"' | gcc -E - | grep -E "typedef.*(Yep8s|Yep8u|Yep16s|Yep16u|Yep32f|Yep32s|Yep32u|Yep64f|Yep64s|Yep64u|YepSize)
*/
typedef int8_t   Yep8s;
typedef uint8_t  Yep8u;
typedef int16_t  Yep16s;
typedef uint16_t Yep16u;
typedef float    Yep32f;
typedef int32_t  Yep32s;
typedef uint32_t Yep32u;
typedef double   Yep64f;
typedef int64_t  Yep64s;
typedef uint64_t Yep64u;
typedef size_t   YepSize;

/* Structs */
enum YepStatus {
 YepStatusOk = 0,
 YepStatusNullPointer = 1,
 YepStatusMisalignedPointer = 2,
 YepStatusInvalidArgument = 3,
 YepStatusInvalidData = 4,
 YepStatusInvalidState = 5,
 YepStatusUnsupportedHardware = 6,
 YepStatusUnsupportedSoftware = 7,
 YepStatusInsufficientBuffer = 8,
 YepStatusOutOfMemory = 9,
 YepStatusSystemError = 10,
 YepStatusAccessDenied = 11
};

struct YepLibraryVersion {
    /** @brief The major version. Library releases with the same major versions are guaranteed to be API- and ABI-compatible. */
    Yep32u major;
    /** @brief The minor version. A change in minor versions indicates addition of new features, and major bug-fixes. */
    Yep32u minor;
    /** @brief The patch level. A version with a higher patch level indicates minor bug-fixes. */
    Yep32u patch;
    /** @brief The build number. The build number is unique for the fixed combination of major, minor, and patch-level versions. */
    Yep32u build;
    /** @brief A UTF-8 string with a human-readable name of this release. May contain non-ASCII characters. */
    const char* releaseName;
};

enum YepCpuArchitecture {
    /** @brief      Instruction set architecture is not known to the library. */
    /** @details    This value is never returned on supported architectures. */
    YepCpuArchitectureUnknown = 0,
    /** @brief      x86 or x86-64 ISA. */
    YepCpuArchitectureX86 = 1,
    /** @brief      ARM ISA. */
    YepCpuArchitectureARM = 2,
    /** @brief      MIPS ISA. */
    YepCpuArchitectureMIPS = 3,
    /** @brief      PowerPC ISA. */
    YepCpuArchitecturePowerPC = 4,
    /** @brief      IA64 ISA. */
    YepCpuArchitectureIA64 = 5,
    /** @brief      SPARC ISA. */
    YepCpuArchitectureSPARC = 6
};

struct YepEnergyCounter {
        Yep64u state[6];
};

struct YepRandom_WELL1024a {
    Yep32u state[32];
    Yep32u index;
};
typedef struct YepRandom_WELL1024a random_state;

/* The following code is built by running `build_header.py` */
/* yepLibrary.h */
enum YepStatus  yepLibrary_Init();
enum YepStatus  yepLibrary_Release();
const struct YepLibraryVersion * yepLibrary_GetVersion();
enum YepStatus  yepLibrary_GetCpuIsaFeatures(Yep64u *isaFeatures);
enum YepStatus  yepLibrary_GetCpuSimdFeatures(Yep64u *simdFeatures);
enum YepStatus  yepLibrary_GetCpuSystemFeatures(Yep64u *systemFeatures);
enum YepStatus  yepLibrary_GetCpuVendor(enum YepCpuVendor *vendor);
enum YepStatus  yepLibrary_GetCpuArchitecture(enum YepCpuArchitecture *architecture);
enum YepStatus  yepLibrary_GetCpuMicroarchitecture(enum YepCpuMicroarchitecture *microarchitecture);
enum YepStatus  yepLibrary_GetCpuDataCacheSize(Yep32u level, Yep32u *cacheSize);
enum YepStatus  yepLibrary_GetCpuInstructionCacheSize(Yep32u level, Yep32u *cacheSize);
enum YepStatus  yepLibrary_GetLogicalCoresCount(Yep32u *logicalCoresCount);
enum YepStatus  yepLibrary_GetCpuCyclesAcquire(Yep64u *state);
enum YepStatus  yepLibrary_GetCpuCyclesRelease(Yep64u *state, Yep64u *cycles);
enum YepStatus  yepLibrary_GetEnergyCounterAcquire(enum YepEnergyCounterType type, struct YepEnergyCounter *state);
enum YepStatus  yepLibrary_GetEnergyCounterRelease(struct YepEnergyCounter *state, Yep64f *measurement);
enum YepStatus  yepLibrary_GetTimerTicks(Yep64u *ticks);
enum YepStatus  yepLibrary_GetTimerFrequency(Yep64u *frequency);
enum YepStatus  yepLibrary_GetTimerAccuracy(Yep64u *accuracy);
enum YepStatus  yepLibrary_GetString(enum YepEnumeration enumerationType, Yep32u enumerationValue, enum YepStringType stringType, void *buffer, YepSize *length);

/* yepAtomic.h */
enum YepStatus  yepAtomic_Swap_Relaxed_S32uS32u_S32u(volatile Yep32u *value, Yep32u newValue, Yep32u *oldValue);
enum YepStatus  yepAtomic_Swap_Acquire_S32uS32u_S32u(volatile Yep32u *value, Yep32u newValue, Yep32u *oldValue);
enum YepStatus  yepAtomic_Swap_Release_S32uS32u_S32u(volatile Yep32u *value, Yep32u newValue, Yep32u *oldValue);
enum YepStatus  yepAtomic_Swap_Ordered_S32uS32u_S32u(volatile Yep32u *value, Yep32u newValue, Yep32u *oldValue);
enum YepStatus  yepAtomic_CompareAndSwap_Relaxed_S32uS32uS32u(volatile Yep32u *value, Yep32u newValue, Yep32u oldValue);
enum YepStatus  yepAtomic_CompareAndSwap_Acquire_S32uS32uS32u(volatile Yep32u *value, Yep32u newValue, Yep32u oldValue);
enum YepStatus  yepAtomic_CompareAndSwap_Release_S32uS32uS32u(volatile Yep32u *value, Yep32u newValue, Yep32u oldValue);
enum YepStatus  yepAtomic_CompareAndSwap_Ordered_S32uS32uS32u(volatile Yep32u *value, Yep32u newValue, Yep32u oldValue);

/* yepCore.h */
enum YepStatus  yepCore_Add_V8sV8s_V8s(const Yep8s *restrict x, const Yep8s *restrict y, Yep8s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V8sV8s_V16s(const Yep8s *restrict x, const Yep8s *restrict y, Yep16s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V8uV8u_V16u(const Yep8u *restrict x, const Yep8u *restrict y, Yep16u *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V16sV16s_V16s(const Yep16s *restrict x, const Yep16s *restrict y, Yep16s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V16sV16s_V32s(const Yep16s *restrict x, const Yep16s *restrict y, Yep32s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V16uV16u_V32u(const Yep16u *restrict x, const Yep16u *restrict y, Yep32u *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32sV32s_V32s(const Yep32s *restrict x, const Yep32s *restrict y, Yep32s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32sV32s_V64s(const Yep32s *restrict x, const Yep32s *restrict y, Yep64s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32uV32u_V64u(const Yep32u *restrict x, const Yep32u *restrict y, Yep64u *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V64sV64s_V64s(const Yep64s *restrict x, const Yep64s *restrict y, Yep64s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32fV32f_V32f(const Yep32f *restrict x, const Yep32f *restrict y, Yep32f *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V64fV64f_V64f(const Yep64f *restrict x, const Yep64f *restrict y, Yep64f *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V8sS8s_V8s(const Yep8s *restrict x, Yep8s y, Yep8s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V8sS8s_V16s(const Yep8s *restrict x, Yep8s y, Yep16s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V8uS8u_V16u(const Yep8u *restrict x, Yep8u y, Yep16u *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V16sS16s_V16s(const Yep16s *restrict x, Yep16s y, Yep16s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V16sS16s_V32s(const Yep16s *restrict x, Yep16s y, Yep32s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V16uS16u_V32u(const Yep16u *restrict x, Yep16u y, Yep32u *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32sS32s_V32s(const Yep32s *restrict x, Yep32s y, Yep32s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32uS32u_V64u(const Yep32u *restrict x, Yep32u y, Yep64u *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32sS32s_V64s(const Yep32s *restrict x, Yep32s y, Yep64s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V64sS64s_V64s(const Yep64s *restrict x, Yep64s y, Yep64s *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V32fS32f_V32f(const Yep32f *restrict x, Yep32f y, Yep32f *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_V64fS64f_V64f(const Yep64f *restrict x, Yep64f y, Yep64f *restrict sum, YepSize length);
enum YepStatus  yepCore_Add_IV8sV8s_IV8s(Yep8s *restrict x, const Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Add_IV16sV16s_IV16s(Yep16s *restrict x, const Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Add_IV32sV32s_IV32s(Yep32s *restrict x, const Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Add_IV64sV64s_IV64s(Yep64s *restrict x, const Yep64s *restrict y, YepSize length);
enum YepStatus  yepCore_Add_IV32fV32f_IV32f(Yep32f *restrict x, const Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Add_IV64fV64f_IV64f(Yep64f *restrict x, const Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Add_IV8sS8s_IV8s(Yep8s *restrict x, Yep8s y, YepSize length);
enum YepStatus  yepCore_Add_IV16sS16s_IV16s(Yep16s *restrict x, Yep16s y, YepSize length);
enum YepStatus  yepCore_Add_IV32sS32s_IV32s(Yep32s *restrict x, Yep32s y, YepSize length);
enum YepStatus  yepCore_Add_IV64sS64s_IV64s(Yep64s *restrict x, Yep64s y, YepSize length);
enum YepStatus  yepCore_Add_IV32fS32f_IV32f(Yep32f *restrict x, Yep32f y, YepSize length);
enum YepStatus  yepCore_Add_IV64fS64f_IV64f(Yep64f *restrict x, Yep64f y, YepSize length);
enum YepStatus  yepCore_Subtract_V8sV8s_V8s(const Yep8s *restrict x, const Yep8s *restrict y, Yep8s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V8sV8s_V16s(const Yep8s *restrict x, const Yep8s *restrict y, Yep16s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V8uV8u_V16u(const Yep8u *restrict x, const Yep8u *restrict y, Yep16u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V16sV16s_V16s(const Yep16s *restrict x, const Yep16s *restrict y, Yep16s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V16sV16s_V32s(const Yep16s *restrict x, const Yep16s *restrict y, Yep32s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V16uV16u_V32u(const Yep16u *restrict x, const Yep16u *restrict y, Yep32u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32sV32s_V32s(const Yep32s *restrict x, const Yep32s *restrict y, Yep32s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32sV32s_V64s(const Yep32s *restrict x, const Yep32s *restrict y, Yep64s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32uV32u_V64u(const Yep32u *restrict x, const Yep32u *restrict y, Yep64u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V64sV64s_V64s(const Yep64s *restrict x, const Yep64s *restrict y, Yep64s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32fV32f_V32f(const Yep32f *restrict x, const Yep32f *restrict y, Yep32f *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V64fV64f_V64f(const Yep64f *restrict x, const Yep64f *restrict y, Yep64f *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V8sS8s_V8s(const Yep8s *restrict x, Yep8s y, Yep8s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V8sS8s_V16s(const Yep8s *restrict x, Yep8s y, Yep16s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V8uS8u_V16u(const Yep8u *restrict x, Yep8u y, Yep16u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V16sS16s_V16s(const Yep16s *restrict x, Yep16s y, Yep16s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V16sS16s_V32s(const Yep16s *restrict x, Yep16s y, Yep32s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V16uS16u_V32u(const Yep16u *restrict x, Yep16u y, Yep32u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32sS32s_V32s(const Yep32s *restrict x, Yep32s y, Yep32s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32sS32s_V64s(const Yep32s *restrict x, Yep32s y, Yep64s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32uS32u_V64u(const Yep32u *restrict x, Yep32u y, Yep64u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V64sS64s_V64s(const Yep64s *restrict x, Yep64s y, Yep64s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V32fS32f_V32f(const Yep32f *restrict x, Yep32f y, Yep32f *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_V64fS64f_V64f(const Yep64f *restrict x, Yep64f y, Yep64f *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S8sV8s_V8s(Yep8s x, const Yep8s *restrict y, Yep8s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S8sV8s_V16s(Yep8s x, const Yep8s *restrict y, Yep16s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S8uV8u_V16u(Yep8u x, const Yep8u *restrict y, Yep16u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S16sV16s_V16s(Yep16s x, const Yep16s *restrict y, Yep16s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S16sV16s_V32s(Yep16s x, const Yep16s *restrict y, Yep32s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S16uV16u_V32u(Yep16u x, const Yep16u *restrict y, Yep32u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S32sV32s_V32s(Yep32s x, const Yep32s *restrict y, Yep32s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S32sV32s_V64s(Yep32s x, const Yep32s *restrict y, Yep64s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S32uV32u_V64u(Yep32u x, const Yep32u *restrict y, Yep64u *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S64sV64s_V64s(Yep64s x, const Yep64s *restrict y, Yep64s *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S32fV32f_V32f(Yep32f x, const Yep32f *restrict y, Yep32f *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_S64fV64f_V64f(Yep64f x, const Yep64f *restrict y, Yep64f *restrict diff, YepSize length);
enum YepStatus  yepCore_Subtract_IV8sV8s_IV8s(Yep8s *restrict x, const Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_IV16sV16s_IV16s(Yep16s *restrict x, const Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_IV32sV32s_IV32s(Yep32s *restrict x, const Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_IV64sV64s_IV64s(Yep64s *restrict x, const Yep64s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_IV32fV32f_IV32f(Yep32f *restrict x, const Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_IV64fV64f_IV64f(Yep64f *restrict x, const Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_V8sIV8s_IV8s(const Yep8s *restrict x, Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_V16sIV16s_IV16s(const Yep16s *restrict x, Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_V32sIV32s_IV32s(const Yep32s *restrict x, Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_V64sIV64s_IV64s(const Yep64s *restrict x, Yep64s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_V32fIV32f_IV32f(const Yep32f *restrict x, Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_V64fIV64f_IV64f(const Yep64f *restrict x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_IV8sS8s_IV8s(Yep8s *restrict x, Yep8s y, YepSize length);
enum YepStatus  yepCore_Subtract_IV16sS16s_IV16s(Yep16s *restrict x, Yep16s y, YepSize length);
enum YepStatus  yepCore_Subtract_IV32sS32s_IV32s(Yep32s *restrict x, Yep32s y, YepSize length);
enum YepStatus  yepCore_Subtract_IV64sS64s_IV64s(Yep64s *restrict x, Yep64s y, YepSize length);
enum YepStatus  yepCore_Subtract_IV32fS32f_IV32f(Yep32f *restrict x, Yep32f y, YepSize length);
enum YepStatus  yepCore_Subtract_IV64fS64f_IV64f(Yep64f *restrict x, Yep64f y, YepSize length);
enum YepStatus  yepCore_Subtract_S8sIV8s_IV8s(Yep8s x, Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_S16sIV16s_IV16s(Yep16s x, Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_S32sIV32s_IV32s(Yep32s x, Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_S64sIV64s_IV64s(Yep64s x, Yep64s *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_S32fIV32f_IV32f(Yep32f x, Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Subtract_S64fIV64f_IV64f(Yep64f x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Negate_V8s_V8s(const Yep8s *restrict x, Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Negate_V16s_V16s(const Yep16s *restrict x, Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Negate_V32s_V32s(const Yep32s *restrict x, Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Negate_V64s_V64s(const Yep64s *restrict x, Yep64s *restrict y, YepSize length);
enum YepStatus  yepCore_Negate_V32f_V32f(const Yep32f *restrict x, Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Negate_V64f_V64f(const Yep64f *restrict x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Negate_IV8s_IV8s(Yep8s *restrict v, YepSize length);
enum YepStatus  yepCore_Negate_IV16s_IV16s(Yep16s *restrict v, YepSize length);
enum YepStatus  yepCore_Negate_IV32s_IV32s(Yep32s *restrict v, YepSize length);
enum YepStatus  yepCore_Negate_IV64s_IV64s(Yep64s *restrict v, YepSize length);
enum YepStatus  yepCore_Negate_IV32f_IV32f(Yep32f *restrict v, YepSize length);
enum YepStatus  yepCore_Negate_IV64f_IV64f(Yep64f *restrict v, YepSize length);
enum YepStatus  yepCore_Multiply_V8sV8s_V8s(const Yep8s *restrict x, const Yep8s *restrict y, Yep8s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V8sV8s_V16s(const Yep8s *restrict x, const Yep8s *restrict y, Yep16s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V8uV8u_V16u(const Yep8u *restrict x, const Yep8u *restrict y, Yep16u *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V16sV16s_V16s(const Yep16s *restrict x, const Yep16s *restrict y, Yep16s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V16sV16s_V32s(const Yep16s *restrict x, const Yep16s *restrict y, Yep32s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V16uV16u_V32u(const Yep16u *restrict x, const Yep16u *restrict y, Yep32u *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32sV32s_V32s(const Yep32s *restrict x, const Yep32s *restrict y, Yep32s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32sV32s_V64s(const Yep32s *restrict x, const Yep32s *restrict y, Yep64s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32uV32u_V64u(const Yep32u *restrict x, const Yep32u *restrict y, Yep64u *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V64sV64s_V64s(const Yep64s *restrict x, const Yep64s *restrict y, Yep64s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32fV32f_V32f(const Yep32f *restrict x, const Yep32f *restrict y, Yep32f *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V64fV64f_V64f(const Yep64f *restrict x, const Yep64f *restrict y, Yep64f *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V8sS8s_V8s(const Yep8s *restrict x, Yep8s y, Yep8s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V8sS8s_V16s(const Yep8s *restrict x, Yep8s y, Yep16s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V8uS8u_V16u(const Yep8u *restrict x, Yep8u y, Yep16u *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V16sS16s_V16s(const Yep16s *restrict x, Yep16s y, Yep16s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V16sS16s_V32s(const Yep16s *restrict x, Yep16s y, Yep32s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V16uS16u_V32u(const Yep16u *restrict x, Yep16u y, Yep32u *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32sS32s_V32s(const Yep32s *restrict x, Yep32s y, Yep32s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32sS32s_V64s(const Yep32s *restrict x, Yep32s y, Yep64s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32uS32u_V64u(const Yep32u *restrict x, Yep32u y, Yep64u *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V64sS64s_V64s(const Yep64s *restrict x, Yep64s y, Yep64s *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V32fS32f_V32f(const Yep32f *restrict x, Yep32f y, Yep32f *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_V64fS64f_V64f(const Yep64f *restrict x, Yep64f y, Yep64f *restrict product, YepSize length);
enum YepStatus  yepCore_Multiply_IV8sV8s_IV8s(Yep8s *restrict x, const Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Multiply_IV16sV16s_IV16s(Yep16s *restrict x, const Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Multiply_IV32sV32s_IV32s(Yep32s *restrict x, const Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Multiply_IV64sV64s_IV64s(Yep64s *restrict x, const Yep64s *restrict y, YepSize length);
enum YepStatus  yepCore_Multiply_IV32fV32f_IV32f(Yep32f *restrict x, const Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Multiply_IV64fV64f_IV64f(Yep64f *restrict x, const Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Multiply_IV8sS8s_IV8s(Yep8s *restrict x, Yep8s y, YepSize length);
enum YepStatus  yepCore_Multiply_IV16sS16s_IV16s(Yep16s *restrict x, Yep16s y, YepSize length);
enum YepStatus  yepCore_Multiply_IV32sS32s_IV32s(Yep32s *restrict x, Yep32s y, YepSize length);
enum YepStatus  yepCore_Multiply_IV64sS64s_IV64s(Yep64s *restrict x, Yep64s y, YepSize length);
enum YepStatus  yepCore_Multiply_IV32fS32f_IV32f(Yep32f *restrict x, Yep32f y, YepSize length);
enum YepStatus  yepCore_Multiply_IV64fS64f_IV64f(Yep64f *restrict x, Yep64f y, YepSize length);
enum YepStatus  yepCore_Min_V8s_S8s(const Yep8s *restrict v, Yep8s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V8u_S8u(const Yep8u *restrict v, Yep8u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V16s_S16s(const Yep16s *restrict v, Yep16s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V16u_S16u(const Yep16u *restrict v, Yep16u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32s_S32s(const Yep32s *restrict v, Yep32s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32u_S32u(const Yep32u *restrict v, Yep32u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64s_S64s(const Yep64s *restrict v, Yep64s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64u_S64u(const Yep64u *restrict v, Yep64u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32f_S32f(const Yep32f *restrict v, Yep32f *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64f_S64f(const Yep64f *restrict v, Yep64f *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V8sV8s_V8s(const Yep8s *restrict x, const Yep8s *restrict y, Yep8s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V8uV8u_V8u(const Yep8u *restrict x, const Yep8u *restrict y, Yep8u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V16sV16s_V16s(const Yep16s *restrict x, const Yep16s *restrict y, Yep16s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V16uV16u_V16u(const Yep16u *restrict x, const Yep16u *restrict y, Yep16u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32sV32s_V32s(const Yep32s *restrict x, const Yep32s *restrict y, Yep32s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32uV32u_V32u(const Yep32u *restrict x, const Yep32u *restrict y, Yep32u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64sV32s_V64s(const Yep64s *restrict x, const Yep32s *restrict y, Yep64s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64uV32u_V64u(const Yep64u *restrict x, const Yep32u *restrict y, Yep64u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32fV32f_V32f(const Yep32f *restrict x, const Yep32f *restrict y, Yep32f *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64fV64f_V64f(const Yep64f *restrict x, const Yep64f *restrict y, Yep64f *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V8sS8s_V8s(const Yep8s *restrict x, Yep8s y, Yep8s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V8uS8u_V8u(const Yep8u *restrict x, Yep8u y, Yep8u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V16sS16s_V16s(const Yep16s *restrict x, Yep16s y, Yep16s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V16uS16u_V16u(const Yep16u *restrict x, Yep16u y, Yep16u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32sS32s_V32s(const Yep32s *restrict x, Yep32s y, Yep32s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32uS32u_V32u(const Yep32u *restrict x, Yep32u y, Yep32u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64sS32s_V64s(const Yep64s *restrict x, Yep32s y, Yep64s *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64uS32u_V64u(const Yep64u *restrict x, Yep32u y, Yep64u *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V32fS32f_V32f(const Yep32f *restrict x, Yep32f y, Yep32f *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_V64fS64f_V64f(const Yep64f *restrict x, Yep64f y, Yep64f *restrict minimum, YepSize length);
enum YepStatus  yepCore_Min_IV8sV8s_IV8s(Yep8s *restrict x, const Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV8uV8u_IV8u(Yep8u *restrict x, const Yep8u *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV16sV16s_IV16s(Yep16s *restrict x, const Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV16uV16u_IV16u(Yep16u *restrict x, const Yep16u *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV32sV32s_IV32s(Yep32s *restrict x, const Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV32uV32u_IV32u(Yep32u *restrict x, const Yep32u *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV64sV32s_IV64s(Yep64s *restrict x, const Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV64uV32u_IV64u(Yep64u *restrict x, const Yep32u *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV32fV32f_IV32f(Yep32f *restrict x, const Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV64fV64f_IV64f(Yep64f *restrict x, const Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Min_IV8sS8s_IV8s(Yep8s *restrict x, Yep8s y, YepSize length);
enum YepStatus  yepCore_Min_IV8uS8u_IV8u(Yep8u *restrict x, Yep8u y, YepSize length);
enum YepStatus  yepCore_Min_IV16sS16s_IV16s(Yep16s *restrict x, Yep16s y, YepSize length);
enum YepStatus  yepCore_Min_IV16uS16u_IV16u(Yep16u *restrict x, Yep16u y, YepSize length);
enum YepStatus  yepCore_Min_IV32sS32s_IV32s(Yep32s *restrict x, Yep32s y, YepSize length);
enum YepStatus  yepCore_Min_IV32uS32u_IV32u(Yep32u *restrict x, Yep32u y, YepSize length);
enum YepStatus  yepCore_Min_IV64sS32s_IV64s(Yep64s *restrict x, Yep32s y, YepSize length);
enum YepStatus  yepCore_Min_IV64uS32u_IV64u(Yep64u *restrict x, Yep32u y, YepSize length);
enum YepStatus  yepCore_Min_IV32fS32f_IV32f(Yep32f *restrict x, Yep32f y, YepSize length);
enum YepStatus  yepCore_Min_IV64fS64f_IV64f(Yep64f *restrict x, Yep64f y, YepSize length);
enum YepStatus  yepCore_Max_V8s_S8s(const Yep8s *restrict v, Yep8s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V8u_S8u(const Yep8u *restrict v, Yep8u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V16s_S16s(const Yep16s *restrict v, Yep16s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V16u_S16u(const Yep16u *restrict v, Yep16u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32s_S32s(const Yep32s *restrict v, Yep32s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32u_S32u(const Yep32u *restrict v, Yep32u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64s_S64s(const Yep64s *restrict v, Yep64s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64u_S64u(const Yep64u *restrict v, Yep64u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32f_S32f(const Yep32f *restrict v, Yep32f *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64f_S64f(const Yep64f *restrict v, Yep64f *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V8sV8s_V8s(const Yep8s *restrict x, const Yep8s *restrict y, Yep8s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V8uV8u_V8u(const Yep8u *restrict x, const Yep8u *restrict y, Yep8u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V16sV16s_V16s(const Yep16s *restrict x, const Yep16s *restrict y, Yep16s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V16uV16u_V16u(const Yep16u *restrict x, const Yep16u *restrict y, Yep16u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32sV32s_V32s(const Yep32s *restrict x, const Yep32s *restrict y, Yep32s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32uV32u_V32u(const Yep32u *restrict x, const Yep32u *restrict y, Yep32u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64sV32s_V64s(const Yep64s *restrict x, const Yep32s *restrict y, Yep64s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64uV32u_V64u(const Yep64u *restrict x, const Yep32u *restrict y, Yep64u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32fV32f_V32f(const Yep32f *restrict x, const Yep32f *restrict y, Yep32f *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64fV64f_V64f(const Yep64f *restrict x, const Yep64f *restrict y, Yep64f *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V8sS8s_V8s(const Yep8s *restrict x, Yep8s y, Yep8s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V8uS8u_V8u(const Yep8u *restrict x, Yep8u y, Yep8u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V16sS16s_V16s(const Yep16s *restrict x, Yep16s y, Yep16s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V16uS16u_V16u(const Yep16u *restrict x, Yep16u y, Yep16u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32sS32s_V32s(const Yep32s *restrict x, Yep32s y, Yep32s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32uS32u_V32u(const Yep32u *restrict x, Yep32u y, Yep32u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64sS32s_V64s(const Yep64s *restrict x, Yep32s y, Yep64s *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64uS32u_V64u(const Yep64u *restrict x, Yep32u y, Yep64u *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V32fS32f_V32f(const Yep32f *restrict x, Yep32f y, Yep32f *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_V64fS64f_V64f(const Yep64f *restrict x, Yep64f y, Yep64f *restrict maximum, YepSize length);
enum YepStatus  yepCore_Max_IV8sV8s_IV8s(Yep8s *restrict x, const Yep8s *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV8uV8u_IV8u(Yep8u *restrict x, const Yep8u *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV16sV16s_IV16s(Yep16s *restrict x, const Yep16s *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV16uV16u_IV16u(Yep16u *restrict x, const Yep16u *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV32sV32s_IV32s(Yep32s *restrict x, const Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV32uV32u_IV32u(Yep32u *restrict x, const Yep32u *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV64sV32s_IV64s(Yep64s *restrict x, const Yep32s *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV64uV32u_IV64u(Yep64u *restrict x, const Yep32u *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV32fV32f_IV32f(Yep32f *restrict x, const Yep32f *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV64fV64f_IV64f(Yep64f *restrict x, const Yep64f *restrict y, YepSize length);
enum YepStatus  yepCore_Max_IV8sS8s_IV8s(Yep8s *restrict x, Yep8s y, YepSize length);
enum YepStatus  yepCore_Max_IV8uS8u_IV8u(Yep8u *restrict x, Yep8u y, YepSize length);
enum YepStatus  yepCore_Max_IV16sS16s_IV16s(Yep16s *restrict x, Yep16s y, YepSize length);
enum YepStatus  yepCore_Max_IV16uS16u_IV16u(Yep16u *restrict x, Yep16u y, YepSize length);
enum YepStatus  yepCore_Max_IV32sS32s_IV32s(Yep32s *restrict x, Yep32s y, YepSize length);
enum YepStatus  yepCore_Max_IV32uS32u_IV32u(Yep32u *restrict x, Yep32u y, YepSize length);
enum YepStatus  yepCore_Max_IV64sS32s_IV64s(Yep64s *restrict x, Yep32s y, YepSize length);
enum YepStatus  yepCore_Max_IV64uS32u_IV64u(Yep64u *restrict x, Yep32u y, YepSize length);
enum YepStatus  yepCore_Max_IV32fS32f_IV32f(Yep32f *restrict x, Yep32f y, YepSize length);
enum YepStatus  yepCore_Max_IV64fS64f_IV64f(Yep64f *restrict x, Yep64f y, YepSize length);
enum YepStatus  yepCore_Sum_V32f_S32f(const Yep32f *restrict v, Yep32f *restrict sum, YepSize length);
enum YepStatus  yepCore_Sum_V64f_S64f(const Yep64f *restrict v, Yep64f *restrict sum, YepSize length);
enum YepStatus  yepCore_SumAbs_V32f_S32f(const Yep32f *restrict v, Yep32f *restrict sumAbs, YepSize length);
enum YepStatus  yepCore_SumAbs_V64f_S64f(const Yep64f *restrict v, Yep64f *restrict sumAbs, YepSize length);
enum YepStatus  yepCore_SumSquares_V32f_S32f(const Yep32f *restrict v, Yep32f *restrict sumSquares, YepSize length);
enum YepStatus  yepCore_SumSquares_V64f_S64f(const Yep64f *restrict v, Yep64f *restrict sumSquares, YepSize length);
enum YepStatus  yepCore_DotProduct_V32fV32f_S32f(const Yep32f *restrict x, const Yep32f *restrict y, Yep32f *restrict dotProduct, YepSize length);
enum YepStatus  yepCore_DotProduct_V64fV64f_S64f(const Yep64f *restrict x, const Yep64f *restrict y, Yep64f *restrict dotProduct, YepSize length);

/* yepMath.h */
enum YepStatus  yepMath_Log_V64f_V64f(const Yep64f *restrict x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepMath_Exp_V64f_V64f(const Yep64f *restrict x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepMath_Sin_V64f_V64f(const Yep64f *restrict x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepMath_Cos_V64f_V64f(const Yep64f *restrict x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepMath_Tan_V64f_V64f(const Yep64f *restrict x, Yep64f *restrict y, YepSize length);
enum YepStatus  yepMath_EvaluatePolynomial_V32fV32f_V32f(const Yep32f *restrict coef, const Yep32f *restrict x, Yep32f *restrict y, YepSize coefCount, YepSize length);
enum YepStatus  yepMath_EvaluatePolynomial_V64fV64f_V64f(const Yep64f *restrict coef, const Yep64f *restrict x, Yep64f *restrict y, YepSize coefCount, YepSize length);

/* yepRandom.h */
enum YepStatus  yepRandom_WELL1024a_Init(struct YepRandom_WELL1024a *restrict state);
enum YepStatus  yepRandom_WELL1024a_Init_V32u(struct YepRandom_WELL1024a *restrict state, const Yep32u seed[32]);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform__V8u(struct YepRandom_WELL1024a *restrict state, Yep8u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform__V16u(struct YepRandom_WELL1024a *restrict state, Yep16u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform__V32u(struct YepRandom_WELL1024a *restrict state, Yep32u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform__V64u(struct YepRandom_WELL1024a *restrict state, Yep64u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S8sS8s_V8s(struct YepRandom_WELL1024a *restrict state, Yep8s supportMin, Yep8s supportMax, Yep8s *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S16sS16s_V16s(struct YepRandom_WELL1024a *restrict state, Yep16s supportMin, Yep16s supportMax, Yep16s *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S32sS32s_V32s(struct YepRandom_WELL1024a *restrict state, Yep32s supportMin, Yep32s supportMax, Yep32s *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S64sS64s_V64s(struct YepRandom_WELL1024a *restrict state, Yep64s supportMin, Yep64s supportMax, Yep64s *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S8uS8u_V8u(struct YepRandom_WELL1024a *restrict state, Yep8u supportMin, Yep8u supportMax, Yep8u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S16uS16u_V16u(struct YepRandom_WELL1024a *restrict state, Yep16u supportMin, Yep16u supportMax, Yep16u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S32uS32u_V32u(struct YepRandom_WELL1024a *restrict state, Yep32u supportMin, Yep32u supportMax, Yep32u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateDiscreteUniform_S64uS64u_V64u(struct YepRandom_WELL1024a *restrict state, Yep64u supportMin, Yep64u supportMax, Yep64u *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateUniform_S32fS32f_V32f_Acc32(struct YepRandom_WELL1024a *restrict state, Yep32f supportMin, Yep32f supportMax, Yep32f *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateFPUniform_S32fS32f_V32f(struct YepRandom_WELL1024a *restrict state, Yep32f supportMin, Yep32f supportMax, Yep32f *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateUniform_S64fS64f_V64f_Acc32(struct YepRandom_WELL1024a *restrict state, Yep64f supportMin, Yep64f supportMax, Yep64f *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateUniform_S64fS64f_V64f_Acc64(struct YepRandom_WELL1024a *restrict state, Yep64f supportMin, Yep64f supportMax, Yep64f *restrict samples, YepSize length);
enum YepStatus  yepRandom_WELL1024a_GenerateFPUniform_S64fS64f_V64f(struct YepRandom_WELL1024a *restrict state, Yep64f supportMin, Yep64f supportMax, Yep64f *restrict samples, YepSize length);

